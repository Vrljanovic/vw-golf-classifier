package mms.golfclassifier.client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.net.ConnectException;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class MainActivity extends AppCompatActivity {

    private static final String url = "http://10.0.2.2:8080";
    public byte[] imageBytes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendImage(View view){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }

    @Override
        protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap image = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                imageBytes = new byte[baos.toByteArray().length];
                imageBytes = baos.toByteArray();
                ImageView imageView = findViewById(R.id.imageView);
                imageView.setImageBitmap(image);
                TextView resultTextView = findViewById(R.id.resultTextView);
                resultTextView.setText("Processing image...");
                closeContextMenu();
                resultTextView.postDelayed(
                new Thread(){
                    @Override
                    public void run(){
                        try {
                            SendImage sendImage = new SendImage(imageBytes);
                            String result = sendImage.execute(url).get();
                            TextView resultTextView = findViewById(R.id.resultTextView);
                            resultTextView.setText("Result: " + result);
                        }catch (Exception ex){
                            TextView resultTextView = findViewById(R.id.resultTextView);
                            resultTextView.setText(ex.getMessage());
                            resultTextView.setText("Could not get answer from server");
                        }
                    }
                }, 10);
            } catch (Exception e) {
                TextView resultTextView = findViewById(R.id.resultTextView);
                resultTextView.setText("Error while getting answer from server.");
            }
        }
    }
}

class SendImage extends AsyncTask<String, Void, String> {
    private byte[] imageBytes;

    SendImage(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            HttpClient http = new DefaultHttpClient();
            HttpPost method = new HttpPost(params[0]);
            method.setEntity(new ByteArrayEntity(imageBytes));
            HttpResponse response = http.execute(method);
            byte[] resp = new byte[1024];
            response.getEntity().getContent().read(resp);
            return new String(resp);
        } catch(ConnectException ex){
            ex.printStackTrace();
            return "Can not connect to server. Try again later.";
        } catch(Exception ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }
    }
}
