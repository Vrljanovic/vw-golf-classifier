from imageai.Detection import ObjectDetection
import numpy as np

# %% Set detector

model_path = 'yolo.h5'
car_detector = ObjectDetection()
car_detector.setModelTypeAsYOLOv3()
car_detector.setModelPath(model_path)
car_detector.CustomObjects(car=True)
car_detector.loadModel()


# %% Detect car from image


def get_car_pixels(filename):
    detections = car_detector.detectObjectsFromImage(input_image=filename, input_type='file',
                                                     extract_detected_objects=True, output_type='array',
                                                     minimum_percentage_probability=70, thread_safe=True)
    if len(detections[2]) == 0:
        return None
    shape_products = []
    for i in range(0, len(detections[2])):
        shape_products.append(detections[2][i].shape[0] * detections[2][i].shape[1])
    for i in range(0, len(detections[1])):
        if detections[1][i]['name'] != 'car':
            detections[2].remove(detections[2][i])
            shape_products.remove(shape_products[i])
    if len(shape_products) == 0:
        return None
    pixels = detections[2][np.argmax(shape_products)]
    if pixels.shape[0] != 0 and pixels.shape[1] != 0:
        return pixels
    return None
