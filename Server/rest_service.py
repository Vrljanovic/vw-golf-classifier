from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.applications.vgg19 import VGG19, preprocess_input
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.backend import set_session
from tensorflow.keras.models import Model
from train_regressor import get_regressor
from car_detection import get_car_pixels
from flask_restful import Resource, Api
from flask import Flask, request
from skimage.io import imsave
from time import time
import tensorflow
import os


app = Flask('GolfClassifier')
api = Api(app)

svc = get_regressor()

uploaded_images = 'UploadedImages'


HOST = '127.0.0.1'
PORT = 9000


class GolfClassifier(Resource):

    def post(self):
        filename = os.path.join(uploaded_images, 'img_' + str(time()) + '.jpg')
        f = open(filename, 'wb')
        print(request.get_data())
        f.write(request.get_data())
        f.close()
        pixels = get_car_pixels(filename)
        if pixels is None:
            return 'There is no car on image.', 200
        imsave(filename, pixels)
        image = load_img(filename, target_size=(224, 224))
        image = img_to_array(image)
        image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
        image = preprocess_input(image)
        global session          
        global graph
        global model
        with graph.as_default():
            set_session(session)
            features = model.predict(image)
            pred = svc.predict(features.reshape(1, -1))[0]
        print('VW Golf MK' + str(pred))
        return 'VW Golf MK' + str(pred), 201


if __name__ == '__main__':
    session = tensorflow.Session()
    set_session(session)
    base_model = VGG19(weights='imagenet', include_top=False)
    b5 = GlobalAveragePooling2D()(base_model.get_layer('block5_pool').output)
    model = Model(inputs=base_model.input, outputs=b5)
    graph = tensorflow.get_default_graph()
    api.add_resource(GolfClassifier, '/')
    app.run(host=HOST, port=8080)



