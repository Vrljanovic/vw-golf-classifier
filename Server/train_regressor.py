from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.applications.vgg19 import VGG19, preprocess_input
from tensorflow.keras.layers import GlobalAveragePooling2D
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from tensorflow.keras.utils import normalize
from tensorflow.keras.models import Model
from pathlib import Path
import pickle
import os


base_model = VGG19(weights='imagenet', include_top=False)
b5 = GlobalAveragePooling2D()(base_model.get_layer('block5_pool').output)
model = Model(inputs=base_model.input, outputs=b5)


def get_image_features(image_path):
    image = load_img(image_path, target_size=(224, 224, 3))
    image = img_to_array(image)
    image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
    image = preprocess_input(image)
    features = model.predict(image)
    return normalize(features.flatten()).flatten()


def read_images(path):
    train = []
    targets = []
    for dirpath, dirs, files in os.walk(path):
        for f in files:
            fname = os.path.join(dirpath, f)
            print(fname)
            train.append(get_image_features(fname))
            targets.append(int(fname.split('\\')[-2].replace('MK', '')))
    return train, targets


def get_regressor():
    path = Path('trained_regressor.sav')
    if not path.is_file():
        return train_regressor(train_size=0.7)
    return pickle.load(open(path, 'rb'))


def train_regressor(train_size):
    train, targets = read_images('VW Golf - Dataset')
    train_set, val_set, train_targets, val_targets = train_test_split(train, targets, train_size=train_size,
                                                                      shuffle=True, stratify=targets)
    param_grid = {'C': [1, 10, 100, 1000],
                  'solver': ['liblinear', 'lbfgs', 'newton-cg', 'sag', 'saga'],
                  'max_iter': [1000000],
                  'multi_class': ['ovr', 'auto', 'multinomial']}
    grid_search = GridSearchCV(LogisticRegression(), param_grid=param_grid, refit=True, verbose=3)
    grid_search.fit(train_set, train_targets)
    preds = grid_search.predict(val_set)
    print(grid_search.best_params_)
    print(grid_search.best_estimator_)
    print(grid_search.best_score_)
    print(confusion_matrix(val_targets, preds))
    pickle.dump(grid_search.best_estimator_, open('trained_regressor.sav', 'wb'))
    return grid_search.best_estimator_